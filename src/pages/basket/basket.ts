import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { YourDetails } from "../your-details/your-details";
import { Helper } from "../../providers/helper";

@Component({
  selector: 'page-basket',
  templateUrl: 'basket.html',
  providers: [Helper]
})
export class Basket {

  basket: any;
  currency: any;
  total: number = 0;
  basketempty: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private toastCtrl: ToastController) {
    this.currency = Helper.currency;
    this.basket = [];
  }

  ionViewWillEnter() {
    this.total = Helper.total;
    this.basket = Helper.basket;
    console.log('==== ionViewWillEnter: Basket items ====');
    Helper.basket.forEach((item, key) => {
      console.log('key '+key);
      console.log(item.title);
      console.log(item.price);
    });

    if (Helper.globalBadge > 0) {
      this.basketempty = false;
    }
  }

  closeBasket() {
    console.log('Basket close:'+Helper.getGlobalBadge());
    this.viewCtrl.dismiss(Helper.getGlobalBadge());
  }

  pushYourdetails() {
    this.navCtrl.push(YourDetails);
  }

  removeItemStorage(itemname, title) {
    Helper.removeItemStorage(itemname, title);
    this.presentToast(title+' removed', 3000, 'bottom');
    this.basket = Helper.basket;
    this.total = Helper.total;
  }

  presentToast(message, duration, position) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }
}
