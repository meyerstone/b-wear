import { Component } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NativeStorage } from 'ionic-native';
import { Http, Headers } from "@angular/http";
import { Folders } from "../folders/folders";
import { Helper } from "../../providers/helper";

@Component({
  templateUrl: 'your-details.html',
  providers: [Helper]
})

export class YourDetails {

  form : FormGroup;
  email: any;
  basketMessage: any;
  http: Http;
  mailgunUrl: string;
  mailgunApiKey: string;
  sender: string;
  recipient: string;
  subject: string;
  isSubmitted: boolean = false;
  submitMessage: String;

  constructor(fb : FormBuilder, http: Http, public loadingCtrl: LoadingController, public navCtrl: NavController) {

    this.http = http;
    this.mailgunUrl = "https://api.mailgun.net/v3/sandbox045083184ee848b78857ac2a1113fe60.mailgun.org";
    this.mailgunApiKey = window.btoa("api:key-7556faf10e6729f6e2bf163d34a7952c");
    this.sender = 'bwear.info@sandbox045083184ee848b78857ac2a1113fe60.mailgun.org';
    this.recipient = 'bwear.info@gmail.com';
    this.subject = 'Mavric Sport and Fashion Order';

    this.form = fb.group({
      yourDetails: fb.group({
            name: ['',Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            contact_number: ['',Validators.compose([Validators.pattern('/^0(6|7|8){1}[0-9]{1}[0-9]{7}$/'), Validators.required])],
            email: ['',Validators.compose([Validators.pattern("/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/"), Validators.required])],
            comments: ''
      }),
      yourAddress: fb.group({
              line1: ['',Validators.compose([Validators.required])],
              line2: ['',Validators.compose([Validators.required])],
              line3: ['',Validators.compose([Validators.required])]
        })
    });

  }

  goHome() {
    this.navCtrl.push(Folders);
  }

  processForm() {

    NativeStorage.getItem('basket')
      .then(
        data => {
          let loading = this.loadingCtrl.create({
            content: 'Sending...'
          });
          loading.present();

          this.basketMessage = '';

          for (let item of Helper.basket) {
            this.basketMessage = this.basketMessage + '<p>'+item.title+'<br>'+Helper.currency+item.price+'</p>';
          }

          let name = this.form.value.yourDetails.name;
          let contact_number = this.form.value.yourDetails.contact_number;
          let email = this.form.value.yourDetails.email;
          let comments = this.form.value.yourDetails.comments;

          let line1 = this.form.value.yourAddress.line1;
          let line2 = this.form.value.yourAddress.line2;
          let line3 = this.form.value.yourAddress.line3;

          this.email = '<p>Hi '+name+', thank you for your order!</p>' +
            '<h3>Contact information</h3>' +
            '<div>Name: '+name+'</div>' +
            '<div>Contact Number: '+contact_number+'</div>' +
            '<div>Email: '+email+'</div>' +
            '<div>Comments: '+comments+'</div>' +
            '<br/>' +
            '<h3>Address information</h3>' +
            '<div>line1: '+line1+'</div>' +
            '<div>line2: '+line2+'</div>' +
            '<div>line3: '+line3+'</div>' +
            '<br/>' +
            '<h3> Order details </h3>' +
            this.basketMessage +
            '';

          let requestHeaders = new Headers();

          requestHeaders.append("Authorization", "Basic " + this.mailgunApiKey);
          requestHeaders.append("Content-Type", "application/x-www-form-urlencoded");
          requestHeaders.append('Access-Control-Allow-Origin', '*');

          this.http.post(
            this.mailgunUrl + "/messages",
            "from="+this.sender+"&to="+this.recipient+"&subject="+this.subject+"&html="+this.email,
            {headers: requestHeaders}
          ).subscribe(success => {
              this.submitMessage = 'Thank you for your order! You will be contacted soon.';
              loading.dismiss();
              Helper.resetBasket();
            }, error => {
              this.submitMessage = 'Sorry, something went wrong, please try again later!';
              loading.dismiss();
            });

          this.isSubmitted = true;

        }
      );
  }

}
