import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Dropbox } from '../../providers/dropbox';
import { ItemDetail } from "../item-detail/item-detail";
import { Helper } from "../../providers/helper";
import { Basket } from "../basket/basket";

@Component({
  selector: 'page-items',
  templateUrl: 'items.html',
  providers: [Dropbox, Helper]
})

export class Items {

  imagepath: any;
  origItems: any;
  items: any;
  itemsName: any;
  itemsPath: any;
  itemsjsonlink: any;
  sharedlinks: any;
  accesstoken: string;
  currency: any;
  searchTerm: string = '';
  search: string;
  loading: any;
  badge: number;

  constructor(public navCtrl: NavController, public params: NavParams, public dropbox: Dropbox, 
              public loadingCtrl: LoadingController, public http: Http,
              public modalCtrl: ModalController) {

    this.itemsName = this.params.get('name');
    this.itemsPath = this.params.get('path');
    this.accesstoken = this.dropbox.getAccessToken();
    this.currency = Helper.currency;
    this.badge = 0;

  }

  ionViewWillEnter() {
    this.badge = Helper.getGlobalBadge();
  }

  ionViewDidLoad() {
    this.getAllItems();
  }

  setFilteredItems() {
    // if the seach string is empty, then assign items to its original values
    if (this.searchTerm != '') {
        this.items = this.filterItems(this.searchTerm);
    } else {
        this.items = this.origItems;
    }
 
  }

  filterItems(searchTerm){
    return this.items.filter((item) => {
      return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });     
  } 

  getAllItems() {

    this.loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    this.loading.present();

    // Get the shared link for the items.json file in current path
    this.dropbox.getSharedlink(this.itemsPath + '/items.json').subscribe(data => {
      this.sharedlinks = data.links;

      // Only 1 link will return but it returns an object of [links], so iterate
      for(let link of this.sharedlinks) {

        // replace the public domain with the api one
        let pattern = /www.dropbox.com/i;
        this.itemsjsonlink = link.url.replace( pattern, "dl.dropboxusercontent.com" );

        // take the shared link returned and call it
        this.getItems(this.itemsjsonlink).subscribe(data => {

          // assign the returned data to items object
          this.items = data;
          this.origItems = this.items;

          // get the images inside each of the item's image folders
          for(let item of this.items) {

            let imagespath = this.itemsPath +'/'+ item.folder_alias;

            // get the temporary link of each image returned above (link lasts for 4 hours) and assign it to the item
            this.dropbox.getTempLink(imagespath + '/thumbnail.jpg').subscribe(data => {

              this.imagepath = data.link;
              item.urlthumb = this.imagepath;
              item.fullpath = imagespath;
              item.desc = item.description;

              this.loading.dismiss();

            }, (err) => {
              console.log(err);
            });

          }

        }, (err) => {
          console.log(err);
        });

      }

    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });

  }

  getItems(link) {

    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http.post(link, {headers: headers})
      .map(res => res.json());

  }

  itemTapped(title, price, description, fullpath) {
    this.navCtrl.push(ItemDetail, { title: title, price: price, desc: description, fullpath: fullpath});
  }

  viewBasket() {
    let modal = this.modalCtrl.create(Basket);
    modal.onDidDismiss(() => {
      this.badge = Helper.getGlobalBadge();
    });
    modal.present();
  }

}
