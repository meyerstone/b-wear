import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Dropbox } from '../../providers/dropbox';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  providers: [Dropbox]
})
export class About {

  accesstoken: string;
  links: any;
  link: any;
  html: any;

  constructor(public navCtrl: NavController, public params: NavParams, public dropbox: Dropbox, public loadingCtrl: LoadingController, public http: Http) {

    this.accesstoken = this.dropbox.getAccessToken();

  }

  ionViewDidLoad() {

    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    loading.present();

    this.dropbox.getSharedlink('/Admin/about.html').subscribe(data => {

      this.links = data.links;

      for(let link of this.links) {

        let pattern = /www.dropbox.com/i;
        this.link = link.url.replace(pattern, "dl.dropboxusercontent.com");

        this.getHtml(this.link).subscribe(data => {

          this.html = data['_body'];
          loading.dismiss();

        }, (err) => {
          console.log(err);
        });

      }

    }, (err) => {
      console.log(err);
    });

  }

  getHtml(link) {

    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http.post(link, {headers: headers})
      .map(res => res);

  }

}
