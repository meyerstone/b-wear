import { Component } from '@angular/core';
import { ModalController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Dropbox } from "../../providers/dropbox";
import { Basket } from "../basket/basket";
import { Helper } from "../../providers/helper";

@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html',
  providers: [Dropbox, Helper]
})
export class ItemDetail {

  title: any;
  price: any;
  desc: any;
  fullpath: any;
  images: any;
  originalurls: any;
  accesstoken: string;
  badge: number;
  currency: any;

  constructor(public params: NavParams, public dropbox: Dropbox, 
              public loadingCtrl: LoadingController, public http: Http, private toastCtrl: ToastController, 
              public modalCtrl: ModalController) {

    this.title = this.params.get('title');
    this.price = this.params.get('price');
    this.desc = this.params.get('desc');
    this.fullpath = this.params.get('fullpath')+'/images';
    this.accesstoken = this.dropbox.getAccessToken();
    this.originalurls = [];
    this.badge = 0;
    this.currency = Helper.currency;

  }

  ionViewWillEnter() {
    this.badge = Helper.getGlobalBadge();
  }

  ionViewDidLoad() {

    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    loading.present();

    this.getImages(this.fullpath).subscribe(data => {

     this.images = data.entries;

      for (let image of this.images) {

        this.dropbox.getTempLink(image.path_lower).subscribe(data => {

          this.originalurls.push(data.link);

        }, (err) => {
          console.log(err);
        });

      }

     loading.dismiss();

     }, (err) => {
     console.log(err);
     loading.dismiss();
     });

  }

  getImages(path) {

    let headers = new Headers();

    headers.append('Authorization', 'Bearer ' + this.accesstoken);
    headers.append('Content-Type', 'application/json');

    let folderPath;

    if(typeof(path) == "undefined" || !path){

      folderPath = {
        path: "",
        "recursive" : false
      };

    } else {

      folderPath = {
        path: path,
        "recursive" : false
      };

    }

    return this.http.post('https://api.dropboxapi.com/2-beta-2/files/list_folder', JSON.stringify(folderPath), {headers: headers})
      .map(res => res.json());

  }

  addItemStorage(itemname, title, price) {
    Helper.addItemToBasket(itemname, title, price);
    this.badge = Helper.getGlobalBadge();
    this.presentToast(title+' added', 3000, 'bottom');
  }

  presentToast(message, duration, position) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }

  viewBasket() {
    let modal = this.modalCtrl.create(Basket);
    modal.onDidDismiss(() => {
      this.badge = Helper.getGlobalBadge();
    });
    modal.present();
  }

}
