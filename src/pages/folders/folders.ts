import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Dropbox } from '../../providers/dropbox';
import { Items } from "../items/items";
import { Helper } from "../../providers/helper";
import { Basket } from "../basket/basket";

@Component({
  selector: 'page-folders',
  templateUrl: 'folders.html',
  providers: [Dropbox, Helper]
})
export class Folders {

  depth: number = 0;
  folders: any;
  path_lower: any;
  img: any;
  accesstoken: string;
  itemsPage = Items;
  level: number = 0;
  badge: number;

  constructor(public navCtrl: NavController, public dropbox: Dropbox, public loadingCtrl: LoadingController,
              public http: Http, public navParams: NavParams, public modalCtrl: ModalController) {

    this.img = {};
    this.img.response = '';
    this.accesstoken = this.dropbox.getAccessToken();
    this.badge = 0;
    Helper.setInitialBadge();

  }

  ionViewWillEnter() {
    this.badge = Helper.getGlobalBadge();
  }

  ionViewDidLoad() {

    this.folders = [];

    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });

    loading.present();

    this.dropbox.getFolders().subscribe(data => {
      this.folders = data.entries;
      this.level = this.level += 1;
      loading.dismiss();
    }, (err) => {
      console.log(err);
    });

  }

  openFolder(path){

    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });

    loading.present();

    this.dropbox.getFolders(path).subscribe(data => {
      this.folders = data.entries;
      this.level = this.level += 1;

      for(let folder of this.folders) {

        if (folder.name == 'items.json')
          continue;

      }

      this.depth++;
      loading.dismiss();
    }, err => {
      console.log(err);
    });

  }

  goBack(){

    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });

    loading.present();

    this.dropbox.goBackFolder().subscribe(data => {
      this.folders = data.entries;
      this.level = this.level -= 1;
      this.depth--;
      loading.dismiss();
    }, err => {
      console.log(err);
    });

  }

  itemTapped(name, path_lower) {
    this.navCtrl.push(Items, {name: name, path: path_lower});
  }

  viewBasket() {
    let modal = this.modalCtrl.create(Basket);
    modal.onDidDismiss(() => {
      this.badge = Helper.getGlobalBadge();
    });
    modal.present();
  }

}
