import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Folders } from '../pages/folders/folders';
import { Dropbox } from '../providers/dropbox';
import { Items } from "../pages/items/items";
import { ItemDetail } from "../pages/item-detail/item-detail";
import { Basket } from "../pages/basket/basket";
import { About } from "../pages/about/about";
import { YourDetails } from "../pages/your-details/your-details";
import { Contact } from "../pages/contact/contact";
import { Helper } from "../providers/helper";

@NgModule({
  declarations: [
    MyApp,
    Folders,
    Items,
    ItemDetail,
    Basket,
    About,
    Contact,
    YourDetails
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Folders,
    Items,
    ItemDetail,
    Basket,
    About,
    Contact,
    YourDetails
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, [Dropbox, Helper]]
})
export class AppModule {}
