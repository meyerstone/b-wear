import { Injectable } from '@angular/core';
import { NativeStorage } from 'ionic-native';

@Injectable()
export class Helper {

  static currency: any = 'R';
  static basket: any;
  static globalBadge: number = 0;
  static total: number = 0;
  static modifiedBasket: any;

  constructor() {}

  static getGlobalBadge() {
    return this.globalBadge;
  }

  static setInitialBadge(){
    NativeStorage.getItem('basket')
      .then(
        data => {
          Helper.basket = data;
          Helper.globalBadge = Helper.basket.length;
        },
          error => {
          Helper.basket = error;
        }
      );
  }

  static addItemToBasket(itemname, title, price) {
    if (!(Helper.basket.length > 0)) {
      /* Check if the array has been initialized. if not then initialize.
       * Initialize the array here instead of in the constructor. The problem with using it in the constructor, everytime
       * a class adds public helper: Helper to their constructor, then the constructor of the helper class is called and 
       * setting the Helper.basket back to an empty []
       */
      Helper.basket = [];
    }

    Helper.basket.push({"title":title, "price":price});
    NativeStorage.setItem(itemname, Helper.basket);
    Helper.globalBadge = Helper.basket.length;
    console.log('==== Basket items ====');
    Helper.total = 0;
    Helper.basket.forEach((item, key) => {
      console.log('total: '+Helper.total);
      Helper.total = Number(Helper.total) + Number(item.price);
      console.log('key '+key);
      console.log(item.title);
      console.log(item.price);
    });
  }

  static removeItemStorage(itemname, title) {

    //reset total and initiate new array
    Helper.total = 0;
    this.modifiedBasket = [];

    Helper.basket.forEach((item, key) => {

      console.log(item);
      console.log(key);
      console.log(title);
      console.log(item.title);

      if (item.title != title) {
        this.modifiedBasket.push({"title":item.title, "price":item.price});
        Helper.total = Number(Helper.total) + Number(item.price);
      }

    });

    // Overwrite new basket
    Helper.basket = this.modifiedBasket;
    NativeStorage.setItem(itemname, Helper.basket);
    Helper.globalBadge = Helper.basket.length;
  }

  static resetBasket() {
    // reset basket
    NativeStorage.clear()
      .catch(
      err => console.log('Cought error on your-details page on clear basket', err)
    );
  }
}
