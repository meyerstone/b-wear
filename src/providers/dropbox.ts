import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class Dropbox {

  accessToken: any;
  folderHistory: any = [];
  data: any;

  static get parameters() {
    return [[Http]];
  }

  constructor(public http: Http) {
    this.data = null;
    this.accessToken = 'sBkG526oY1AAAAAAAAAACQGhbUFvvoU2gdYHwZ5SRBK4cTomj9oZVGPuIw_B8T5P';
  }

  getAccessToken() {
    return this.accessToken;
  }

  getFolders(path?){

    let headers = new Headers();

    headers.append('Authorization', 'Bearer ' + this.accessToken);
    headers.append('Content-Type', 'application/json');

    let folderPath;

    if(typeof(path) == "undefined" || !path){

      folderPath = {
        path: ""
      };

    } else {

      folderPath = {
        path: path
      };

      if(this.folderHistory[this.folderHistory.length - 1] != path){
        this.folderHistory.push(path);
      }

    }

    return this.http.post('https://api.dropboxapi.com/2-beta-2/files/list_folder', JSON.stringify(folderPath), {headers: headers})
        .map(res => res.json());

  }

  getSharedlink(path?) {

    let itemsPath;

    if(typeof(path) == "undefined" || !path){

      itemsPath = {
        path: ""
      };

    } else {

      itemsPath = {
        path: path
      };

    }

    let headers = new Headers();
    headers.append('Authorization', 'Bearer ' + this.accessToken);
    headers.append('Content-Type', 'application/json');

    return this.http.post('https://api.dropboxapi.com/2/sharing/get_shared_links', JSON.stringify(itemsPath), {headers: headers})
        .map(res => res.json());

  }

  goBackFolder(){

    if(this.folderHistory.length > 0){

      this.folderHistory.pop();
      let path = this.folderHistory[this.folderHistory.length - 1];

      return this.getFolders(path);
    }
    else {
      return this.getFolders();
    }
  }

  getTempLink(url) {

    let headers = new Headers();

    headers.append('Authorization', 'Bearer ' + this.accessToken);
    headers.append('Content-Type', 'application/json');

    let linkPath;

    if(typeof(url) == "undefined" || !url){

      linkPath = {
        path: ""
      };

    } else {

      linkPath = {
        path: url
      };

    }

    return this.http.post('https://api.dropboxapi.com/2/files/get_temporary_link', JSON.stringify(linkPath), {headers: headers})
      .map(res => res.json());

  }

}
